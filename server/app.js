console.log("Hello World 2 ss");
var x = 9;
y = x + 1;
console.log(y);
// this is the code 
/*
    This is the whoe story 
    method that add up x and y
*/

var name = "Kenneth Phang";
console.log(typeof(name));
var age = 25.56666666;
console.log(typeof(age));
var smart = false;
console.log(typeof(smart));

var user_profile = {
    name: "Kenneth",
    age: 39,
    gender: 'M'
};

console.log("User profile name --> " + user_profile.name);
console.log("User profile age --> " + user_profile.age);

var property = {
    address: "sengkang",
    price: 1000000,
    size: 1003
};

var fruits = [
    "Orange",
    false,
    "Durian",
    3
];

console.log(fruits[1]);
var x = 4;
// Condition
if (x > 10) {
    console.log("More than 10");
} else if (x > 5 && x < 7) {
    console.log("In Between 5 and 7");
} else {
    console.log("Otherwise");
}
var x = 2;
switch (x) {
    case 1:
        console.log("This is 1");
        break;
    case 2:
        console.log("This is 2");
        break;
    default:
        console.log("Otherwise");
}

// js callback

function hi(name) {
    console.log("Hello ! -- " + name);
}

function hi2(name) {
    console.log("Hello2 ! -- " + name);
}

function sayHi(callback, callback2, name) {
    console.log("Inside sayHi");
    callback(name);
    callback2(name);
}

sayHi(hi, hi2, "Kenneth Phang");

var i = 5;
while (i < 10) {
    console.log(++i);
    console.log(" ^^^^^^^ ");
}

var i = 7;
do {
    console.log(i++);
} while (i < 10);


var fruits = [
    "Orange",
    "Apple",
    "Durian",
    "Banana"
];
console.log("---> " + fruits[3]);
console.log("---> " + fruits[fruits.length - 1]);
console.log("---> " + fruits.length);
console.log(fruits);
console.log(typeof(fruits));

for (var i = 0; i < fruits.length; i++) {
    console.log("----------------------");
    console.log(i);
    console.log(fruits[i]);
}

var pos = fruits.indexOf('Banana');
console.log(pos);

var posOfApple = fruits.indexOf('Apple');
console.log(posOfApple);


function printFruits(fruit_, index_1) {
    console.log("Index > " + index_1);
    console.log("Fruit > " + fruit_);
}

fruits.forEach(printFruits);

console.log("*************************");
fruits.forEach(function(element, index_1) {
    console.log("Index > " + element);
    console.log("Fruit > " + index_1);
});

console.log(fruits);
fruits.push("rambutan");
console.log(fruits);
fruits.pop();
console.log(fruits);
fruits.splice(1, 1);
console.log(fruits);
fruits.splice(1, 4);
console.log(fruits);

var fruits2 = [
    "Orange",
    "Apple",
    "Durian",
    "Banana"
];

var yy = fruits2.slice(1, 3); // start , end
// index = 1 start off at Apple end with 3 -1 Durian
console.log(">> " + fruits2);
console.log(">> " + yy);

fruits2.shift();
console.log(fruits2);
fruits2.unshift("New Banana");
console.log(fruits2);
fruits2.sort();
console.log(fruits2);

var numbers = [4, 2, 5, 1, 3];
numbers.sort();
console.log(numbers);
console.log(">> Start >>>");
numbers.sort(function(a, b) {
    console.log(a);
    console.log(b);
    return b - a;
});
console.log(numbers);
console.log(">> end >>>");

// [1, 2, 3, 4, 5]

var func = function() {
    console.log("return some val");
    return 1;
};
func();
console.log(func);
console.log(func());

var obj = {
    name: "Kenneth Phang",
    age: 30,
    myJob: "Software Engineer",
    "my vehicle": "car",
    hello: function() {
        console.log("Say Hi " + name);
    }
};

console.log(obj.name, obj.age, obj.myJob);
console.log(obj['name'], obj['age'], obj['my vehicle']);

obj.hello();


var author = {
    name: "Kenneth"
};

var publisher = {
    name: "SPH"
};

var book = {
    name: "JS from novice to ninja",
    author: author,
    publisher: publisher,
    noOfCopies: 100
};

console.log(book.author);
console.log(book.publisher);
console.log(book.noOfCopies);
delete book.noOfCopies
console.log(book.noOfCopies);
console.log("---------------------------");

function Person(name) {
    this.name = name;
    this.gender = "Male";
    var job = "Engineer";
    console.log("inside the person object scope " +
        this.gender);
}
var job = "Engineer";

var person = new Person("Kenneth Phang");
console.log(person.name);
console.log(person.job);
console.log("global scope --> " + job);
console.log("Outside the person object scope " +
    person.gender);

function Person() {

}
var person = new Person();
console.log(person instanceof Person);
console.log(person instanceof Object);
console.log({}
    instanceof Object);

console.log(typeof(person));

//ES6

 // Default Parameters in ES6
 
 var link = function (height, color, url) {
    var height = height || 50
    var color = color || 'red'
    var url = url || 'http://azat.co'
}

var link = function(height = 50, color = 'red', url = 'http://azat.co') {
}

// Template Literals in ES6
var name = 'Your name is ' + first + ' ' + last + '.'
var url = 'http://localhost:3000/api/messages/' + id

var name = `Your name is ${first} ${last}.`
var url = `http://localhost:3000/api/messages/${id}`

//Multi-line Strings in ES6
var roadPoem = 'Then took the other, as just as fair,\n\t'
    + 'And having perhaps the better claim\n\t'
    + 'Because it was grassy and wanted wear,\n\t'
    + 'Though as for that the passing there\n\t'
    + 'Had worn them really about the same,\n\t'

var fourAgreements = 'You have the right to be you.\n\
    You can only be you when you do your best.'
    
var roadPoem = `Then took the other, as just as fair,
    And having perhaps the better claim
    Because it was grassy and wanted wear,
    Though as for that the passing there
    Had worn them really about the same,`

var fourAgreements = `You have the right to be you.
    You can only be you when you do your best.`
    
//Destructuring Assignment in ES6
var data = $('body').data(), // data has properties house and mouse
  house = data.house,
  mouse = data.mouse
  
var jsonMiddleware = require('body-parser').json

var body = req.body, // body has username and password
  username = body.username,
  password = body.password  
  
var {house, mouse} = $('body').data() // we'll get house and mouse variables

var {json: jsonMiddleware} = require('body-parser')

var {username, password} = req.body

var [col1, col2]  = $('.column'),
  [line1, line2, line3, , line5] = file.split('\n')
  
//Enhanced Object Literals in ES6
var serviceBase = {port: 3000, url: 'azat.co'},
    getAccounts = function(){return [1,2,3]}

var accountServiceES5 = {
  port: serviceBase.port,
  url: serviceBase.url,
  getAccounts: getAccounts,
  toString: function() {
    return JSON.stringify(this.valueOf())
  },
  getUrl: function() {return "http://" + this.url + ':' + this.port},
  valueOf_1_2_3: getAccounts()
}

var accountServiceES5ObjectCreate = Object.create(serviceBase)
var accountServiceES5ObjectCreate = {
  getAccounts: getAccounts,
  toString: function() {
    return JSON.stringify(this.valueOf())
  },
  getUrl: function() {return "http://" + this.url + ':' + this.port},
  valueOf_1_2_3: getAccounts()
}

var serviceBase = {port: 3000, url: 'azat.co'},
    getAccounts = function(){return [1,2,3]}
var accountService = {
    __proto__: serviceBase,
    getAccounts,
    
toString() {
     return JSON.stringify((super.valueOf()))
    },
    getUrl() {return "http://" + this.url + ':' + this.port},
    [ 'valueOf_' + getAccounts().join('_') ]: getAccounts()
};
console.log(accountService)

// Arrow Functions in ES6
var _this = this
$('.btn').click(function(event){
  _this.sendData()
})

$('.btn').click((event) =>{
  this.sendData()
})

var logUpperCase = function() {
  var _this = this

  this.string = this.string.toUpperCase()
  return function () {
    return console.log(_this.string)
  }
}

logUpperCase.call({ string: 'es6 rocks' })()


var logUpperCase = function() {
  this.string = this.string.toUpperCase()
  return () => console.log(this.string)
}

logUpperCase.call({ string: 'es6 rocks' })()

var ids = ['5632953c4e345e145fdf2df8','563295464e345e145fdf2df9']
var messages = ids.map(function (value) {
  return "ID is " + value // explicit return
})

var ids = ['5632953c4e345e145fdf2df8','563295464e345e145fdf2df9']
var messages = ids.map(value => `ID is ${value}`) // implicit return

var ids = ['5632953c4e345e145fdf2df8', '563295464e345e145fdf2df9'];
var messages = ids.map(function (value, index, list) {
  return 'ID of ' + index + ' element is ' + value + ' ' // explicit return
})

var ids = ['5632953c4e345e145fdf2df8','563295464e345e145fdf2df9']
var messages = ids.map((value, index, list) => `ID of ${index} element is ${value} `) // implicit return

// Promises in ES6
setTimeout(function(){
  console.log('Yay!')
}, 1000)

var wait1000 =  new Promise(function(resolve, reject) {
  setTimeout(resolve, 1000)
}).then(function() {
  console.log('Yay!')
})

var wait1000 =  new Promise((resolve, reject)=> {
  setTimeout(resolve, 1000)
}).then(()=> {
  console.log('Yay!')
})

setTimeout(function(){
  console.log('Yay!')
  setTimeout(function(){
    console.log('Wheeyee!')
  }, 1000)
}, 1000)

var wait1000 =  ()=> new Promise((resolve, reject)=> {setTimeout(resolve, 1000)})

wait1000()
  .then(function() {
    console.log('Yay!')
    return wait1000()
  })
  .then(function() {
    console.log('Wheeyee!')
  })
  
// Block-Scoped Constructs Let and Const
// Note: Technically, let and const variables declarations are being hoisted too, but not their assignation. 
// Since they're made so that they can't be used before assignation, it intuitively feels like there is no hoisting, but there is. 
// Find out more on this very detailed explanation here if you want to know more.
function calculateTotalAmount (vip) {
  var amount = 0
  if (vip) {
    var amount = 1
  }
  { // more crazy blocks!
    var amount = 100
    {
      var amount = 1000
      }
  }  
  return amount
}

// Classes in ES6
class baseModel {
  constructor(options = {}, data = []) { // class constructor
    this.name = 'Base'
    this.url = 'http://azat.co/api'
    this.data = data
    this.options = options
  }

    getName() { // class method
      console.log(`Class name: ${this.name}`)
    }
}

let accounts = new AccountModel(5)
accounts.getName()
console.log('Data is %s', accounts.accountsData)

// Modules in ES6
// module.js
module.exports = {
  port: 3000,
  getAccounts: function() {
  }
}


var service = require('module.js')
console.log(service.port) // 3000

import {port, getAccounts} from 'module'
console.log(port) // 3000

import * as service from 'module'
console.log(service.port) // 3000

// ES6 try and catch 
try {
  try {
    throw new Error('oops');
  }
  catch (ex) {
    console.error('inner', ex.message);
  }
  finally {
    console.log('finally');
  }
}
catch (ex) {
  console.error('outer', ex.message);
}
console.log(calculateTotalAmount(true))

function calculateTotalAmount (vip) {
  var amount = 0 // probably should also be let, but you can mix var and let
  if (vip) {
    let amount = 1 // first amount is still 0
  } 
  { // more crazy blocks!
    let amount = 100 // first amount is still 0
    {
      let amount = 1000 // first amount is still 0
      }
  }  
  return amount
}

console.log(calculateTotalAmount(true))

function calculateTotalAmount (vip) {
  const amount = 0  
  if (vip) {
    const amount = 1 
  } 
  { // more crazy blocks!
    const amount = 100 
    {
      const amount = 1000
      }
  }  
  return amount
}

console.log(calculateTotalAmount(true))

    